document.getElementById("hello_text").textContent = "はじめてのJavaScript";

var count=0;

var cells; //盤面を示す変数

//ブロックのパターン
var blocks = {
	i: {
		class:"i",
		pattern: [
			[1,1,1,1]
		]
	},
	o: {
		class:"o",
		pattern: [
			[1,1],
			[1,1]
		]
	},
	t: {
		class:"t",
		pattern: [
			[0,1,0],
			[1,1,1]
		]
	},
	s: {
		class:"s",
		pattern: [
			[0,1,1],
			[1,1,0]
		]
	},
	z: {
		class:"z",
		pattern: [
			[1,1,0],
			[0,1,1]
		]
	},
	j: {
		class:"j",
		pattern: [
			[1,0,0],
			[1,1,1]
		]
	},
	l: {
		class:"l",
		pattern: [
			[0,0,1],
			[1,1,1]
		]
	},

}

loadTable(); //読み込み
setInterval(function() {
	count++;
	document.getElementById("hello_text").textContent="はじめてのJavaScript("+count+")";
// ブロックが積み上がり切っていないかのチェック
/*
for (var row = 0; row < 2; row++) {
	for (var col = 0; col < 10; col++) {
		if (cells[row][col].className !== "") {
			alert("game over");
		}
	}
}
*/
	if (hasFallingBlock()) {  //落下中のブロックの確認
		fallBlocks();  //あればブロックを落とす
	} else {  //なければ
		deleteRow();  //揃っている行を消す
		generateBlock();  //新ブロック作成
	}
},1000);

//ここから関数宣言

function loadTable() {
	cells=[];
	var td_array=document.getElementsByTagName("td");
	var index=0;
	for (var row=0; row<20; row++) {
		cells[row]=[]; //2次元配列をつくる
		for (var col=0; col<10; col++) {
			cells[row][col]=td_array[index];
			index++;
		}
	}
}

function fallBlocks() {
	//1.一番下の行にブロックがあればfalseに
	for (var col=0; col<10; col++) {
		if (cells[19][col].blockNum===fallingBlockNum) {
			isFalling=false;
			return; //一番下の行にブロックがいるので落とさない
		}
	}
	//2.1マス下に別のブロックがあれば落とさない
	for (var row=18; row>=0; row--) {
		for (var col=0; col<10; col++) {
			if (cells[row][col].blockNum === fallingBlockNum) {
				if (cells[row+1][col].className!=="" && cells[row+1][col].blockNum!==fallingBlockNum){
					isFalling=false;
					return;
				}
			}
		}
	}
	//下から二番目の行から順次クラスを下げる
	for (var row=18; row>=0; row--) {
		for (var col=0; col<10; col++) {
			if (cells[row][col].blockNum === fallingBlockNum) {
				cells[row+1][col].className=cells[row][col].className;
				cells[row+1][col].blockNum=cells[row][col].blockNum;
				cells[row][col].className="";
				cells[row][col].blockNum=null;
			}
		}
	}
}

var isFalling=false;
function hasFallingBlock() {
  // 落下中のブロックがあるか確認する
  return isFalling;
}

function deleteRow() {
  // そろっている行を消す
	for (var row = 19; row >= 0; row--) {
     var canDelete = true;
     for (var col = 0; col < 10; col++) {
       if (cells[row][col].className === "") {
         canDelete = false;
       }
     }
     if (canDelete) {
       // 1行消す
       for (var col = 0; col < 10; col++) {
         cells[row][col].className = "";
       }
       // 上の行のブロックをすべて1マス落とす(1行上)
       for (var downRow = row - 1; row >= 0; row--) {
         for (var col = 0; col < 10; col++) {
           cells[downRow + 1][col].className = cells[downRow][col].className;
           cells[downRow + 1][col].blockNum = cells[downRow][col].blockNum;
           cells[downRow][col].className = "";
           cells[downRow][col].blockNum = null;
         }
       }
     }
  }
}

var fallingBlockNum=0;
function generateBlock() {
	// ランダムにブロックを生成する
	//1.ブロックのパターン決定
	var keys = Object.keys(blocks);
	var nextBlockKey = keys[Math.floor(Math.random()*keys.length)];
	var nextBlock=blocks[nextBlockKey];
	var nextFallingBlockNum=fallingBlockNum+1;
	//2.選んだパターンのブロック配置
var pattern = nextBlock.pattern;
for (var row=0; row<pattern.length; row++) {
	for (var col=0; col<pattern[row].length; col++) {
		if (pattern[row][col]) {
			cells[row][col+3].className=nextBlock.class;
			cells[row][col+3].blockNum=nextFallingBlockNum;
		}
	}
}
	//3.ブロック落下中に変更
	isFalling=true;
	fallingBlockNum = nextFallingBlockNum;
}

//キーボードイベントの監視
document.addEventListener("keydown", onKeyDown);

//キー入力から関数呼び出し
function onKeyDown(event) {
	if (event.keyCode === 37) {
		moveLeft();
	} else if (event.keyCode === 39) {
		moveRight();
	}
}

function moveRight() {
  // ブロックを右に移動させる
  for (var row = 0; row < 20; row++) {
    for (var col = 9; col >= 0; col--) {
      if (cells[row][col].blockNum === fallingBlockNum) {
        cells[row][col + 1].className = cells[row][col].className;
        cells[row][col + 1].blockNum = cells[row][col].blockNum;
        cells[row][col].className = "";
        cells[row][col].blockNum = null;
      }
    }
  }
}

function moveLeft() {
  // ブロックを左に移動させる
  for (var row = 0; row < 20; row++) {
    for (var col = 0; col < 10; col++) {
      if (cells[row][col].blockNum === fallingBlockNum) {
        cells[row][col - 1].className = cells[row][col].className;
        cells[row][col - 1].blockNum = cells[row][col].blockNum;
        cells[row][col].className = "";
        cells[row][col].blockNum = null;
      }
    }
  }
}
